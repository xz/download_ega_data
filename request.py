from urllib.request import urlopen, urlretrieve, Request
import pandas as pd
from urllib.parse import urlencode
import re
import json
import colors
import shutil
import sys
from os import path
import os


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def info(msg, *args, **kwargs):
    eprint(colors.blue(msg), *args, **kwargs)


def debug(msg, *args, **kwargs):
    eprint(colors.green(msg), *args, **kwargs)


def call_api(path, data, base_path='https://ega.ebi.ac.uk/ega/rest/access/v2/', method='POST'):
    url = f"{base_path}{path}"
    encoded_data = bytes(urlencode(data), 'utf-8')

    info(f"[API] {url}")

    whole_res = json.loads(
        urlopen(Request(
            url,
            data=encoded_data,
            headers={'Accept': 'application/json'},
            method=method,
        )).read()
    )

    return whole_res['response']['result']


def login():
    res = call_api('users/login', {'loginrequest': {"username": "tianying%40hawaii.edu", "password": "xunzhu98"}})
    session_id = res[1]
    return session_id


def list_datasets(session_id):
    res = call_api('datasets', {'session': session_id})
    dataset_list = res
    debug(f"Found {len(dataset_list)} datasets: {', '.join(dataset_list)}")
    return dataset_list


def list_files(dataset_id, session_id):
    res = call_api(f"datasets/{dataset_id}/files", {'session': session_id})

    files_df = pd.DataFrame.from_records(res)

    return files_df


def get_tickets(session_id):
    res = call_api(f"requests", {'session': session_id})

    tickets_df = pd.DataFrame.from_records(res)

    return tickets_df


def remove_request(request_labels):
    for request_label in request_labels:
        res = call_api(f"requests/delete/{request_label}", {'session': session_id})
        debug(f"res = {res}")


def request_dataset(dataset_id, session_id):
    res = call_api(
        f"requests/new/datasets/{dataset_id}",
        {
            "downloadrequest": {
                "rekey": "12qw",
                "downloadType": "STREAM",
                "descriptor": "request.py",
            },
            'session': session_id,
        },
    )
    debug(f"res = {res}")


def get_sample_meta(dataset_id, session_id):
    res = call_api(
        f"samples?queryBy=dataset&queryId={dataset_id}&limit=0",
        {},
        base_path="https://ega-archive.org/metadata/v2/",
        method='GET',
    )
    sample_meta_df = pd.DataFrame.from_records(res)
    return sample_meta_df


if __name__ == '__main__':
    session_id = login()
    dataset_id = list_datasets(session_id)[0]

    sample_meta_df = get_sample_meta(dataset_id, session_id)
    sample_meta_df.to_csv('sample_meta_df.csv')

    files_df = list_files(dataset_id, session_id)
    files_df.to_csv('files_df.csv')

    # tickets_df = get_tickets(session_id)
    # tickets_df.to_csv('tickets_df.csv')
    # if 'label' in tickets_df.columns:
    #     remove_request(tickets_df['label'].unique())

    # request_dataset(dataset_id, session_id)

    tickets_df = get_tickets(session_id)
    tickets_df.to_csv('tickets_df_all.csv')

    tickets_df = tickets_df.loc[[bool(re.search(r'BS_GL', x)) for x in files_df['fileName']]]
    tickets_df['file_name'] = [path.basename(x) for x in tickets_df['fileName']]
    tickets_df = tickets_df.sort_values('file_name')
    tickets_df.to_csv('tickets_df.csv')

    os.makedirs('downloaded_files', exist_ok=True)
    for id_, row in tickets_df.iterrows():
        info(f"{row['ticket']}  -->  {row['fileName']}")
        with urlopen(
            Request(
                f"http://ega.ebi.ac.uk/ega/rest/ds/v2/downloads/{row['ticket']}",
                headers={'Accept': 'application/octet-stream'},
                method='GET',
            )
        ) as response, open(f"downloaded_files/{row['file_name']}", 'wb') as out_file:
            shutil.copyfileobj(response, out_file)
